import os
import logging
import azure.functions as func
import json

from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS, PointSettings


influxdb_url = os.environ.get("INFLUXDB_URL")

# You can generate a Token from the "Tokens Tab" in the UI
# tokens for influxdb1.8 are "username:password"
influxdb_token = os.environ.get("INFLUXDB_TOKEN")
if not influxdb_token:
    influxdb_token = f'{os.environ.get("INFLUXDB_USER")}:{os.environ.get("INFLUXDB_PASSWORD")}'
influxdb_org = os.environ.get("INFLUXDB_ORG")
# bucket for influxdb1.8 will be "database/rp" or just "database"
influxdb_bucket = os.environ.get("INFLUXDB_BUCKET", os.environ.get("INFLUXDB_DATABASE"))


def main(req: func.HttpRequest) -> func.HttpResponse:

    # create influxdb client
    client = InfluxDBClient(
        url=influxdb_url,
        token=influxdb_token
    )

    point_settings = PointSettings()
    js = req.get_json()
    for k, v in js['head']['environment'].items():
        point_settings.add_default_tag(k, v)
    write_api = client.write_api(write_options=SYNCHRONOUS, point_settings=point_settings)

    fields = js['head']['fields']
    for data_point in js.get('data', []):
        point = Point("cr1000x")
        point.time(data_point['time'])
        if len(data_point['vals']) > len(fields):
            err_msg = f"more data point values than fields definitions"
            logging.error(err_msg)
            return func.HttpResponse(json.dumps({"error": err_msg}), mimetype="application/json", status_code=400)
        for i, v in enumerate(data_point['vals']):
            # skip NAN
            if v == "NAN":
                continue
            point.field(fields[i]['name'], float(v))

        write_api.write(influxdb_bucket, influxdb_org, point)

    return func.HttpResponse(json.dumps({"success": True}), mimetype="application/json", status_code=200)
