import os
import logging
import azure.functions as func

import json
import datetime
import pandas
from pandas.errors import ParserError

from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS, PointSettings


influxdb_url = os.environ.get("INFLUXDB_URL")

# You can generate a Token from the "Tokens Tab" in the UI
# tokens for influxdb1.8 are "username:password"
influxdb_token = os.environ.get("INFLUXDB_TOKEN")
if not influxdb_token:
    influxdb_token = f'{os.environ.get("INFLUXDB_USER")}:{os.environ.get("INFLUXDB_PASSWORD")}'
influxdb_org = os.environ.get("INFLUXDB_ORG")
# bucket for influxdb1.8 will be "database/rp" or just "database"
influxdb_bucket = os.environ.get("INFLUXDB_BUCKET", os.environ.get("INFLUXDB_DATABASE"))


def dateparse(x) -> datetime.datetime:
    """
    parses csis _kinda_ iso datetime

    Args:
        x (str): datetime string

    Returns:
        TYPE: Description
    """
    return datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S')


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.error(influxdb_url)
    if len(req.files) == 0:
        logging.warn("request with no files")
        return func.HttpResponse(f'No files in request', status_code=400)
    # create influxdb client
    client = InfluxDBClient(
        url=influxdb_url,
        token=influxdb_token
    )

    failcount = 0
    succcount = 0
    emptycount = 0
    for k, data_buffer in req.files.items():
        # get station name from url params "station" or fallback "datalogger_name"
        station = req.params.get("station", req.params.get("datalogger_name"))
        psettings = PointSettings()
        try:
            tags = dict()
            data_buffer.seek(0)
            # example header:
            # "TOA5","STN_NAME","CR1000X","15027","CR1000X.Std.04.02","CPU:CR1000X program 30Oct.cr1x","3623","DT_30_Min"
            infodf = pandas.read_csv(data_buffer, nrows=1, header=None)
            names = ("filetype", "station", "model", "serial", "os", "program", "signature", "table")
            for i in infodf:
                if i >= len(names):
                    logging.debug(f"more headers than names in datafile")
                    continue
                tags[names[i]] = str(infodf[i][0]).replace(" ", "_").replace("\'", "").replace("\"", "").replace(",", "").replace(":", "_")
            if tags.get('station') not in (station, None, "STN_NAME"):
                tags['station_file'] = tags.get("station")
            if tags.get('station') in (None, "STN_NAME"):
                tags['station'] = station
            for tk, tv in tags.items():
                if not tv:
                    continue
                psettings.add_default_tag(tk, tv)
        except ParserError as e:
            logging.error(f"ParserError for headers: {str(e)}")
            write_api = client.write_api(write_type=SYNCHRONOUS)
            point = Point("cr1000x_errors")\
                .tag("errortype", "ParserError")\
                .field("error", "error parsing headers")\
                .field("exc", str(e))
            if station:
                point.tag("station", station)
            write_api.write(influxdb_bucket, influxdb_org, point)
            write_api.close()

        write_api = client.write_api()
        try:
            data_buffer.seek(0)
            df = pandas.read_csv(data_buffer,
                                 skiprows=[0, 2, 3],
                                 index_col=0,
                                 parse_dates=True,
                                 infer_datetime_format=True,
                                 na_values=['NAN', "-INF", 7999, -7999],
                                 low_memory=False)
            for pd_ts, row in df.iterrows():
                # logging.error(row)
                # wtf sometimes we get null timestamps, skip them
                if pandas.isnull(pd_ts):
                    continue
                # pytelegraf doesnt support pandas timestamps
                pyts = pd_ts.to_pydatetime()

                point = Point("cr1000x").tag("fn", k).time(pyts)

                for k, v in row.items():
                    ndata = 0
                    # skip nan because influxdb has no concept of nan
                    if pandas.isnull(v):
                        continue

                    try:
                        # dont even try to parse timestamp values
                        ts = dateparse(v)
                        v = ts.timestamp() * 1e9
                    except:
                        pass

                    # set timestamps to int nanos
                    if type(v) == pandas.Timestamp:
                        try:
                            point.field(k, int(v.to_pydatetime().timestamp() * 1e9))
                        except Exception as e:
                            print(e)

                    if type(v) == int and not k == "RECORD":
                        v = float(v)
                    if k == "RECORD":
                        v = int(v)

                    point.field(k, v)
                    ndata += 1

                if ndata > 0:
                    logging.debug(point.to_line_protocol())
                    write_api.write(influxdb_bucket, influxdb_org, point)
                    succcount += 1
                else:
                    logging.debug("no parseable data in point")
                    point = Point("cr1000x_errors")\
                        .tag("errortype", "DataError")\
                        .field("error", "no parseable data in point")
                    write_api.write(influxdb_bucket, influxdb_org, point)
                    emptycount += 1

        except ParserError as e:
            logging.error(f"ParserError for data: {str(e)}")
            point = Point("cr1000x_errors")\
                .tag("errortype", "ParserError")\
                .field("error", "error parsing data")\
                .field("exc", str(e))
            write_api.write(influxdb_bucket, influxdb_org, point)
            failcount += 1
        write_api.close()

    success = {'npoints': succcount, 'nfails': failcount, 'nempty': emptycount,
               'nfiles': len(req.files), 'fnames': list(req.files.keys())}
    return func.HttpResponse(json.dumps(success), status_code=200)
    # if name:
    #     return func.HttpResponse(f"Hello, {name}. This HTTP triggered function executed successfully.")
    # else:
    #     return func.HttpResponse(
    #         "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
    #         status_code=200
    #     )
