#!/usr/bin/env python3
"""
quick python script to format functionapp usage data from azure json
outputs a table with the format of:

timestamp|  FunctionExecutionUnits / FunctionExecutionCount  :   FunctionExecutionUnits per FunctionExecutionCount


"""

import json
import sys
indata = json.loads(sys.stdin.read())
functionunits = indata['value'][0]['timeseries'][0]['data']
functionexecs = indata['value'][1]['timeseries'][0]['data']
print("timestamp          | EU(GB-s) /  EC     |     EU per EC")
#      2021-02-03T07:05:00|   15.13 /  2       |       7.56
tot = 0
for i, x in enumerate(functionunits):
    gbs = x['total'] / 1024000
    tot += gbs
    print(f"{x['timeStamp'].replace('+00:00', '')}| {gbs:7.2f}  / {functionexecs[i]['total']:3d}", end="\t|\t")
    if functionexecs[i]['total'] != 0:
        gbs /= functionexecs[i]['total']
    print(f'{gbs:5.2f}')

print("-----")
cost = tot* 0.000022
print(f'total: \t\t{int(tot):7d}GB-s')
remain = (400*1000)-tot
print(f'free remaining: {int(remain):7d}GB-s')
print(f'cost: \t\t${cost:.2f}')
